{
  * TAD para manejar una lista de codigos y sus frecuencias
}
UNIT UFrequencyList;
INTERFACE
TYPE
  PFrequencyList = ^TFrequencyList;
  TFrequencyList = Array[0..255] of LongWord;

Function FrequencyList_new(): PFrequencyList;
Procedure FrequencyList_free(var l: PFrequencyList);
Procedure FrequencyList_clear(var l: PFrequencyList);
Procedure FrequencyList_inc(var l: PFrequencyList; code: Byte);
Procedure FrequencyList_dec(var l: PFrequencyList; code: Byte);
Function FrequencyList_get(var l: PFrequencyList; code: Byte): LongWord;
Procedure FrequencyList_set(var l: PFrequencyList; code: Byte; count: LongWord);

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function FrequencyList_new(): PFrequencyList;
Begin
  new(FrequencyList_new);

  FrequencyList_clear(FrequencyList_new);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure FrequencyList_free(var l: PFrequencyList);
Begin
  if (assigned(l)) then
    begin
      dispose(l);

      l := NIL;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure FrequencyList_clear(var l: PFrequencyList);
Var
  i: Byte;

Begin
  if (assigned(l)) then
    for i := 0 to 255 do
      l^[i] := 0;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure FrequencyList_inc(var l: PFrequencyList; code: Byte);
Begin
  if (assigned(l)) then
    begin
      inc(l^[code]);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure FrequencyList_dec(var l: PFrequencyList; code: Byte);
Begin
  if (assigned(l)) then
    begin
      dec(l^[code]);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function FrequencyList_get(var l: PFrequencyList; code: Byte): LongWord;
Begin
  FrequencyList_get := 0;

  if (assigned(l)) then
    FrequencyList_get := l^[code];
End;

////////////////////////////////////////////////////////////////////////////////
Procedure FrequencyList_set(var l: PFrequencyList; code: Byte; count: LongWord);
Begin
  if (assigned(l)) then
    begin
      l^[code] := count;
    end;
End;

END.
