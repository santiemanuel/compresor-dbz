{
  * TAD para manejar un arbol de Huffman implementado como una lista enlazada
  * y un arbol binario.
}
UNIT UHuffmanTree;
INTERFACE
USES
  SysUtils,
  UFrequencyList,
  UHuffmanCodeList;

TYPE
  PHuffmanTreeNode = ^THuffmanTreeNode;
  THuffmanTreeNode = Record
    code      : Integer;          { codigo del nodo                           }
    frequency : LongWord;         { frecuencia de aparicion del codigo        }
    left      : PHuffmanTreeNode; { hijo izquierdo del nodo                   }
    right     : PHuffmanTreeNode; { hijo derecho del nodo                     }
    next      : PHuffmanTreeNode; { puntero al elemento siguiente en la lista }
  End;

  PHuffmanTree = PHuffmanTreeNode;

Function HuffmanTree_newFromFrequencyList(var list: PFrequencyList): PHuffmanTree;
Function HuffmanTree_newFromStringSpec(var str: AnsiString): PHuffmanTree;
Procedure HuffmanTree_free(var tree: PHuffmanTree);
Procedure HuffmanTree_clear(var tree: PHuffmanTree);
Function HuffmanTree_getCodeList(var tree: PHuffmanTree): PHuffmanCodeList;
Function HuffmanTree_getTreeSpec(tree: PHuffmanTree): AnsiString;

IMPLEMENTATION

Procedure HuffmanTree_compute(var tree: PHuffmanTree); Forward;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_newFromFrequencyList(var list: PFrequencyList): PHuffmanTree;
  Function getCodeMinFrequency(var list: PFrequencyList): Integer;
  Var
    i   : Byte;
    aux : Integer;

  Begin
    aux := -1;

    for i := 0 to 255 do
      begin
        if (FrequencyList_get(list, i) > 0) then
          begin
            if (aux = -1) then
              aux := i
            else
              if (FrequencyList_get(list, i) < FrequencyList_get(list, aux)) then
                aux := i;
          end;
      end;

    getCodeMinFrequency := aux;
  End;

Var
  code  : Integer;
  node  : PHuffmanTreeNode;
  aux   : PHuffmanTreeNode;

Begin
  HuffmanTree_newFromFrequencyList := NIL;

  if (assigned(list)) then
    begin
      code  := getCodeMinFrequency(list);
      aux   := HuffmanTree_newFromFrequencyList;

      while (code > -1) do
        begin
          new(node);
          node^.code      := code;
          node^.frequency := FrequencyList_get(list, code);
          node^.left      := NIL;
          node^.right     := NIL;
          node^.next      := NIL;

          if (aux = NIL) then
            begin
              HuffmanTree_newFromFrequencyList  := node;
              aux                               := node;
            end
          else
            begin
              aux^.next := node;
              aux       := aux^.next;
            end;

          node := NIL;

          FrequencyList_set(list, code, 0);
          code := getCodeMinFrequency(list);
        end;

      writeLn();
      HuffmanTree_compute(HuffmanTree_newFromFrequencyList);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_newFromStringSpec(var str: AnsiString): PHuffmanTree;
Begin
  new(HuffmanTree_newFromStringSpec);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_free(var tree: PHuffmanTree);
Begin
  if (assigned(tree)) then
    begin
      HuffmanTree_clear(tree);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_clear(var tree: PHuffmanTree);
  procedure clearNode(var node: PHuffmanTreeNode);
  begin
    if (node <> NIL) then
      begin
        clearNode(node^.left);
        clearNode(node^.right);
        dispose(node);
        node := NIL;
      end;
  end;

Var
  node  : PHuffmanTreeNode; { nodo auxiliar para eliminar }

Begin
  if (assigned(tree)) then
    begin
      while (tree <> NIL) do
        begin
          node  := tree;
          tree  := tree^.next;
          clearNode(node);

          dispose(node);
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_getCodeList(var tree: PHuffmanTree): PHuffmanCodeList;
  procedure getCode(var node: PHuffmanTreeNode; code: Byte; value: String;
                    right: Boolean; var list: PHuffmanCodeList);
  begin


    if (node <> NIL) then
      begin
        if (right) then
          value := value + '1'
        else
          value := value + '0';

        getCode(node^.left, node^.code, value, FALSE, list);
        getCode(node^.right, node^.code, value, TRUE, list);
      end
    else
      HuffmanCodeList_set(list, code, value)
  end;

Begin
  HuffmanTree_getCodeList := NIL;

  if (assigned(tree)) then
    begin
      HuffmanTree_getCodeList := HuffmanCodeList_new();

      if (assigned(tree)) then
        begin
          if (assigned(tree^.left)) then
            getCode(tree^.left, tree^.left^.code, '', FALSE,
                    HuffmanTree_getCodeList
                   );

          if (assigned(tree^.right)) then
            getCode(tree^.right, tree^.right^.code, '', TRUE,
                    HuffmanTree_getCodeList
                   );
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_compute(var tree: PHuffmanTree);
Var
  aux       : PHuffmanTreeNode; { nodo auxiliar para insertar                 }
  pos       : PHuffmanTreeNode; { nodo para recorrer el arbol                 }
  prev      : PHuffmanTreeNode; { nodo previo al de recorrido para reconectar }

Begin
  if (assigned(tree)) then
    begin
      while (tree^.next <> NIL) do
        begin
          new(aux);

          aux^.code       := -1;
          aux^.frequency  := tree^.frequency;
          aux^.right      := tree;

          tree := tree^.next;
          inc(aux^.frequency, tree^.frequency);
          aux^.left := tree;

          aux^.next       := NIL;

          tree := tree^.next;

          aux^.left^.next   := NIL;
          aux^.right^.next  := NIL;

          prev  := NIL;
          pos   := tree;

          while ((pos <> NIL) and (aux^.frequency >= pos^.frequency)) do
            begin
              prev  := pos;
              pos   := pos^.next;
            end;

          if (prev = NIL) then
            begin
              aux^.next := tree;
              tree      := aux;
            end
          else
            begin
              if (pos = NIL) then
                begin
                  aux^.next := tree;
                  tree      := aux;
                end
              else
                begin
                  aux^.next   := pos;
                  prev^.next  := aux;
                end;
            end;
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_getTreeSpec(tree: PHuffmanTree): AnsiString;
  Function decToBin(dec: Byte): AnsiString;
  Begin
    if (dec < 2) then
      begin
        if (dec = 0) then
          decToBin := '0'
        else
          decToBin := '1';
      end
    else
      if (dec mod 2 = 0) then
        decToBin := decToBin(dec div 2) + '0'
      else
        decToBin := decToBin(dec div 2) + '1';
  End;

  Function completeString(str: AnsiString): AnsiString;
  Begin
    while (length(str) < 8) do
      str := '0' + str;

    completeString := str;
  End;

  Procedure getCode(tree: PHuffmanTree; var code: AnsiString);
  Begin
    if (tree <> NIL) then
      begin
        if ((tree^.left <> NIL) or (tree^.right <> NIL)) then
          begin
            code := code + '0';
            getCode(tree^.left, code);
            getCode(tree^.right, code);
          end
        else
          begin
            code := code + '1' + completeString(decToBin(tree^.code));
          end;
      end;
  End;

Begin
  HuffmanTree_getTreeSpec := '';

  if (assigned(tree)) then
    begin
      getCode(tree, HuffmanTree_getTreeSpec);
    end
End;

END.
