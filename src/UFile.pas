{
  * TAD para el manejo de archivos.
}
UNIT UFile;
INTERFACE
USES
  UByteBuffer;

TYPE
  TFileMode =
  (
    FILE_MODE_NONE,         { modo no definido                      }
    FILE_MODE_READ,         { modo de lectura                       }
    FILE_MODE_WRITE         { modo de escritura                     }
  );

  PFile = ^TFile;
  TFile = Record
    opened  : Boolean;      { determina si esta abierto el archivo  }
    mode    : TFileMode;    { modo de apertura del archivo          }
    path    : AnsiString;   { direccion ligada al archivo           }
    handle  : File of Byte; { archivo ligado                        }
  End;

Function File_open(path: AnsiString; mode: TFileMode): PFile;
Procedure File_close(var f: PFile);
Function File_read(var f: PFile; count: QWord): PByteBuffer;
Procedure File_write(var f: PFile; var buffer: PByteBuffer; count: QWord);
Function File_opened(var f: PFile): Boolean;
Function File_mode(var f: PFile): TFileMode;
Function File_size(var f: PFile): QWord;
Function File_path(var f: PFile): AnsiString;
Function File_tell(var f: PFile): QWord;
Procedure File_seek(var f: PFile; pos: QWord);

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function File_open(path: AnsiString; mode: TFileMode): PFile;
Var
  aux_file : File of Byte;

Begin
  File_open := NIL;

  if (mode <> FILE_MODE_NONE) then
    begin
      assign(aux_file, path);

      {$I-}
      case (mode) of
        FILE_MODE_READ:
          reset(aux_file);

        FILE_MODE_WRITE:
          rewrite(aux_file);
      end;
      {$I+}

      if (IOResult = 0) then
        begin
          new(File_open);

          File_open^.opened := (IOResult = 0);
          File_open^.mode   := mode;
          File_open^.path   := path;
          File_open^.handle := aux_file;
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure File_close(var f: PFile);
Begin
  if (assigned(f)) then
    begin
      if (f^.opened) then
        begin
          close(f^.handle);

          dispose(f);

          f := NIL;
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function File_read(var f: PFile; count: QWord): PByteBuffer;
Var
  aux_buffer: PByte;

Begin
  File_read := NIL;

  if (assigned(f)) then
    if ((f^.opened) and (f^.mode = FILE_MODE_READ)) then
    begin
      if (count > (File_size(f) - File_tell(f))) then
        count := (File_size(f) - File_tell(f));

      getMem(aux_buffer, count);

      blockRead(f^.handle, aux_buffer^, count);

      File_read := ByteBuffer_new(aux_buffer, count);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure File_write(var f: PFile; var buffer: PByteBuffer; count: QWord);
Begin
  if (assigned(f) and (f^.opened) and (f^.mode = FILE_MODE_WRITE)) then
    begin
      blockWrite(f^.handle, ByteBuffer_data(buffer)^, ByteBuffer_length(buffer));
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function File_opened(var f: PFile): Boolean;
Begin
  File_opened := FALSE;

  if (assigned(f)) then
    File_opened := f^.opened;
End;

////////////////////////////////////////////////////////////////////////////////
Function File_mode(var f: PFile): TFileMode;
Begin
  File_mode := FILE_MODE_NONE;

  if (assigned(f)) then
    File_mode := f^.mode;
End;

////////////////////////////////////////////////////////////////////////////////
Function File_size(var f: PFile): QWord;
Begin
  File_size := 0;

  if (assigned(f)) then
    File_size := fileSize(f^.handle);
End;

////////////////////////////////////////////////////////////////////////////////
Function File_path(var f: PFile): AnsiString;
Begin
  File_path := '';

  if (assigned(f)) then
    File_path := f^.path;
End;

////////////////////////////////////////////////////////////////////////////////
Function File_tell(var f: PFile): QWord;
Begin
  File_tell := 0;

  if (assigned(f) and (File_opened(f))) then
    File_tell := filePos(f^.handle);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure File_seek(var f: PFile; pos: QWord);
Begin
  if (assigned(f) and (File_opened(f))) then
    Seek(f^.handle, pos);
End;

END.
