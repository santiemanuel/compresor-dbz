{
  * TAD para manejar un "buffer" de "byte"
}
UNIT UByteBuffer;
INTERFACE
TYPE
  PByteBuffer = ^TByteBuffer;
  TByteBuffer = Record
    data    : PByte;  { datos del buffer    }
    length  : QWord;  { longitud del buffer }
  End;


Function ByteBuffer_new(var data: PByte; length: QWord): PByteBuffer;
Procedure ByteBuffer_free(var buffer: PByteBuffer);
Function ByteBuffer_data(var buffer: PByteBuffer): PByte;
Function ByteBuffer_length(var buffer: PByteBuffer): QWord;
Function ByteBuffer_get(var buffer: PByteBuffer; pos: QWord): Byte;

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_new(var data: PByte; length: QWord): PByteBuffer;
Begin
  new(ByteBuffer_new);

  ByteBuffer_new^.data    := data;
  ByteBuffer_new^.length  := length;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure ByteBuffer_free(var buffer: PByteBuffer);
Begin
  if (assigned(buffer)) then
    begin
      freeMem(buffer^.data);
      buffer^.length := 0;

      dispose(buffer);

      buffer := NIL;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_data(var buffer: PByteBuffer): PByte;
Begin
  ByteBuffer_data := NIL;

  if (assigned(buffer)) then
    begin
      ByteBuffer_data := buffer^.data;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_length(var buffer: PByteBuffer): QWord;
Begin
  ByteBuffer_length := 0;

  if (assigned(buffer)) then
    begin
      ByteBuffer_length := buffer^.length;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_get(var buffer: PByteBuffer; pos: QWord): Byte;
Begin
  ByteBuffer_get := 0;

  if (assigned(buffer)) then
    begin
      if (pos <= buffer^.length) then
        ByteBuffer_get := buffer^.data[pos - 1];
    end;
End;

END.
