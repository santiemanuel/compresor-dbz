{
  * TAD para manejar el par "simbolo:frequencia" utilizado para
  * crear el arbol de Huffman y obtener los códigos.
}
UNIT UData;
INTERFACE
TYPE
  TData = Record
    symbol    : Byte;     { simbolo a comprimir     }
    frequency : LongWord; { frecuencia de aparicion }
  End;

Procedure Data_set(var data: TData; symbol: Byte; frequency: LongWord);
Procedure Data_get(var data: TData; var symbol: Byte; var frequency: LongWord);
Function Data_symbol(var data: TData): Byte;
Function Data_frequency(var data: TData): LongWord;

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Procedure Data_set(var data: TData; symbol: Byte; frequency: LongWord);
Begin
  data.symbol     := symbol;
  data.frequency  := frequency;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure Data_get(var data: TData; var symbol: Byte; var frequency: LongWord);
Begin
  symbol    := data.symbol;
  frequency := data.frequency;
End;

////////////////////////////////////////////////////////////////////////////////
Function Data_symbol(var data: TData): Byte;
Begin
  Data_symbol := data.symbol;
End;

////////////////////////////////////////////////////////////////////////////////
Function Data_frequency(var data: TData): LongWord;
Begin
  Data_frequency := data.frequency;
End;

END.
