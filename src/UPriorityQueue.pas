{
  * TAD para manejar una lista de prioridad sobre "TData".
}
UNIT UPriorityQueue;
INTERFACE
USES
  UData,
  UFile;

TYPE
  PPriorityQueueNode = ^TPriorityQueueNode;
  TPriorityQueueNode = Record
    data  : TData;                { dato del nodo             }
    next  : PPriorityQueueNode;   { puntero al nodo siguiente }
  End;

  PPriorityQueue = ^TPriorityQueue;
  TPriorityQueue = Record
    first   : PPriorityQueueNode; { puntero al primer elemento de la queuea  }
    last    : PPriorityQueueNode; { puntero al ultimo nodo de la queuea      }
    length  : LongWord;           { longitud/tamaño de la queuea             }
  End;

Function PriorityQueue_new(): PPriorityQueue;
Procedure PriorityQueue_free(var queue: PPriorityQueue);
Function PriorityQueue_empty(var queue: PPriorityQueue): Boolean;
Function PriorityQueue_length(var queue: PPriorityQueue): LongWord;
Procedure PriorityQueue_push(var queue: PPriorityQueue; var data: TData);
Function PriorityQueue_get(var queue: PPriorityQueue; var data: TData): Boolean;
Procedure PriorityQueue_pop(var queue: PPriorityQueue);

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function PriorityQueue_new(): PPriorityQueue;
Begin
  new(PriorityQueue_new);

  PriorityQueue_new^.first  := NIL;
  PriorityQueue_new^.last   := NIL;
  PriorityQueue_new^.length := 0;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure PriorityQueue_free(var queue: PPriorityQueue);
Begin
  while (not PriorityQueue_empty(queue)) do
    PriorityQueue_pop(queue);

  dispose(queue);

  queue := NIL;
End;

////////////////////////////////////////////////////////////////////////////////
Function PriorityQueue_empty(var queue: PPriorityQueue): Boolean;
Begin
  PriorityQueue_empty := (assigned(queue) and (queue^.length = 0));
End;

////////////////////////////////////////////////////////////////////////////////
Function PriorityQueue_length(var queue: PPriorityQueue): LongWord;
Begin
  PriorityQueue_length := 0;

  if (assigned(queue)) then
    PriorityQueue_length := queue^.length;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure PriorityQueue_push(var queue: PPriorityQueue; var data: TData);
  function lessThan(var d1, d2: TData): Boolean;
  begin
    lessThan := Data_frequency(d1) <= Data_frequency(d2);
  end;

Var
  node  : PPriorityQueueNode;  { nodo a insertar                       }
  pos   : PPriorityQueueNode;  { para buscar la posicion a insertar    }
  prev  : PPriorityQueueNode;  { para mantener la referencia anterior  }

Begin
  if (assigned(queue)) then
    begin
      new(node);
      node^.data  := data;
      node^.next  := NIL;

      if (PriorityQueue_empty(queue)) then
        begin
          queue^.first  := node;
          queue^.last   := node;
        end
      else
        begin
          prev  := NIL;
          pos   := queue^.first;

          while ((pos <> NIL) and (lessThan(data, pos^.data))) do
            begin
              prev  := pos;
              pos   := pos^.next;
            end;

          if (pos <> NIL) then
            begin
              if (pos = queue^.first) then
                begin
                  node^.next    := queue^.first;
                  queue^.first  := node;
                end
              else
                begin
                  prev^.next  := node;
                  node^.next  := pos;
                end;
            end
          else
            begin
              queue^.last^.next := node;
              queue^.last       := node;
            end;
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function PriorityQueue_get(var queue: PPriorityQueue; var data: TData): Boolean;
Begin
  PriorityQueue_get := FALSE;

  if (assigned(queue) and not PriorityQueue_empty(queue)) then
    begin
      data := queue^.first^.data;
      PriorityQueue_get := TRUE;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure PriorityQueue_pop(var queue: PPriorityQueue);
Var
  node: PPriorityQueueNode;

Begin
  if (assigned(queue) and not PriorityQueue_empty(queue)) then
    begin
      node := queue^.first;
      queue^.first := queue^.first^.next;

      dec(queue^.length);

      dispose(node);
    end;
End;

END.
